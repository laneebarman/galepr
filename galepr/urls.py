from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from galepr.models import Article
from urllib import parse
from . import views

app_name = 'articles'
urlpatterns = [
	#url(r'^$', ListView.as_view(queryset = Article.objects.all().order_by('-publication'), template_name = "galepr/articleList.html")),
	url(r'^$', views.index, name='article'),
	url(r'^(?P<article_id>\d+)$', views.detail, name='detail')
]